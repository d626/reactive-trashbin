use gpio;
use gpio::{GpioIn, GpioValue};
use std::{thread, time};

pub fn init<F: 'static>(callback: F)
    where F: Fn() + Send {
    //thread::spawn(move || {
    //    loop {
    //        thread::sleep(time::Duration::from_secs(5));
    //        callback();
    //    }
    //});

    let mut gpio2 = gpio::sysfs::SysFsGpioInput::open(2).expect("UNABLE TO OPEN GPIO2");
    let mut prev_value = GpioValue::Low;
    loop {
        let cur_value = gpio2.read_value().expect("UNABLE TO READ GPIO2");
        println!("cur_value: {:?}", cur_value);
        if cur_value == GpioValue::High && prev_value == GpioValue::Low {
            callback();
        }
        prev_value = cur_value;
        thread::sleep(time::Duration::from_millis(100));
    }
}
