extern crate rumqtt; // note: the docs for this crate is outdated, read the source code instead.
extern crate gpio;
use rumqtt::{MqttOptions, MqttClient, QoS};

use std::sync::{Arc, Mutex};

mod level_sensor;
mod lid_sensor;

static ID: &str = "garbagebin/bin1";
static NEW_BINS: &str = "garbagebin/newBinIdRequest";
static BROKER: &str = "test.mosquitto.org:1883";


fn main() {
    let client_options = MqttOptions::new()
                                      .set_keep_alive(5)
                                      .set_reconnect(3)
                                      .set_client_id(ID)
                                      .set_broker(BROKER);

    let mqtt_client = Arc::new(Mutex::new(MqttClient::start(client_options, None).expect("FAILED TO START MQTT CLIENT")));

    (*mqtt_client.lock().unwrap()).publish(NEW_BINS, QoS::Level2, ID.to_string().into_bytes()).expect("UNABLE TO PUBLISH ID");

    // initialize level sensor
    level_sensor::init();

    // initialize lid sensor (this will be the main loop/sleep while waiting for interupts)
    lid_sensor::init(move || {
        let level = level_sensor::get_level();
        (*mqtt_client.lock().unwrap()).publish(ID, QoS::Level0, level.to_string().into_bytes()).expect("UNABLE TO PUBLISH STATUS");
    });

    loop{};
}